import request from 'supertest';
import express from 'express';
import jwt from 'jsonwebtoken';
import bodyParser from 'body-parser';
import contactRouter from '../controllers/contactController';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Simulate user data
const users = [
    { id: 1, username: 'user1', password: 'password1' }
];

// Login route for generating a token
app.post('/login', (req, res) => {
    const { username, password } = req.body;
    const user = users.find(u => u.username === username && u.password === password);
    if (user) {
        const token = jwt.sign({ userId: user.id, username: user.username }, 'secret', { expiresIn: '1h' });
        res.json({ message: 'Authentication successful!', token });
    } else {
        res.status(401).json({ message: 'Authentication failed! Invalid username or password.' });
    }
});

// Middleware to bypass JWT for testing (mocks jwt.verify)
app.use((req, res, next) => {
  jwt.verify = jest.fn().mockReturnValue({ userId: '1', username: 'user1' });
  next();
});

app.use('/contacts', contactRouter);

describe('Authentication and Contacts API', () => {
  let token: string; // explicitly declaring the type as string

  beforeAll(async () => {
    // Login and retrieve token
    const res = await request(app)
      .post('/login')
      .send({ username: 'user1', password: 'password1' });
    token = res.body.token;
  });

  it('should authenticate and receive a token', async () => {
    const res = await request(app)
      .post('/login')
      .send({ username: 'user1', password: 'password1' });
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('token');
  });

  describe('GET /contacts', () => {
    it('should return a list of contacts', async () => {
      const response = await request(app)
        .get('/contacts?page=1&limit=10')
        .set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveLength(10);
    });
  });
});
