// src/models/Contact.ts
import mongoose from 'mongoose';

const contactSchema = new mongoose.Schema({
    username: { type: String, required: true },
    phoneNumber: { type: String, required: true }
});

const Contact = mongoose.model('Contact', contactSchema);

export default Contact;
