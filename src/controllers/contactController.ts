import express, { Request, Response} from 'express';
import Contact from '../models/Contact';
import redis from '../db/redisClient';

const router = express.Router();

router.get('/', async (req: Request, res: Response) => {
    try {
      const { page = 1, limit = 100 } = req.query;
      const cacheKey = `contacts:${page}:${limit}`;
  
      // Try to get contacts from Redis
      redis.get(cacheKey, async (err, cachedContacts) => {
        if (err) throw err;
  
        if (cachedContacts) {
          // If contacts are cached, return them
          return res.json(JSON.parse(cachedContacts));
        } else {
          // If not cached, fetch from database
          const contacts = await Contact.find()
            .limit(Number(limit))
            .skip((Number(page) - 1) * Number(limit))
            .exec();
  
          // Store contacts in Redis for future requests
          redis.setex(cacheKey, 3600, JSON.stringify(contacts)); // Cache for 1 hour
  
          // Return contacts
          res.json(contacts);
        }
      });
    } catch (err) {
      console.error("Error retrieving contacts:", err);
      res.status(500).json({ message: 'Error fetching contacts', error: err });
    }
  });

export default router;
