import dotenv from 'dotenv';
dotenv.config();

import express, { Request, Response } from 'express';
import mongoDb from './db/mongoClient';
import jwt from 'jsonwebtoken';
import contactRouter from './controllers/contactController';

const PORT = process.env.PORT || 3000;
const app = express();

// Mock user data
const users = [
    { id: 1, username: 'user1', password: 'password1' },
    { id: 2, username: 'user2', password: 'password2' }
];

// Middleware to parse JSON bodies
app.use(express.json());
// Use express.urlencoded() to parse URL-encoded bodies
app.use(express.urlencoded({ extended: true }));

// Login endpoint
app.post('/login', (req: Request, res: Response) => {
    const { username, password } = req.body;
    const user = users.find(u => u.username === username && u.password === password);

    if (!user) {
        return res.status(401).json({ message: 'Authentication failed! Invalid username or password.' });
    }

    const token = jwt.sign({ userId: user.id, username: user.username }, process.env.JWT_SECRET || '', { expiresIn: '1h' });
    res.json({ message: 'Authentication successful!', token });
});

// Routes
app.use('/contacts', contactRouter);

// Connect to MongoDB and start the server
mongoDb().then(() => {
    app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
}).catch(err => {
    console.error("Failed to connect to MongoDB:", err);
    process.exit(1);
});

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
    console.log(`Unhandled Rejection at: ${promise} - reason: ${err}`);
});
