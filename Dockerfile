# Use an official Node runtime as a parent image, updated to a newer LTS version
FROM node:16

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install any needed packages specified in package.json
RUN npm install

# Bundle app source inside Docker image
COPY . .

# Make port 3000 available to the world outside this container
EXPOSE 3000

# Copy the .env file into the container
COPY .env ./

# Run the app when the container launches
CMD ["npm", "start"]
