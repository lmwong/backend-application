# Contacts API

This is a simple RESTful Contacts API built with Node.js, Express, MongoDB and Redis. It supports fetching contacts with pagination and is secured with JWT authentication.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Docker
- Docker Compose

### Installing and running with Docker

1. **Clone the repository:**

    ```bash
   git clone git@gitlab.com:lmwong/backend-application.git
   cd backend-application

2. **Set up environment variables:**
    Create a .env file in the root directory with the dotenv file as example

3. **Build the docker containers:**
    ```bash
    docker-compose build

4. **Run the docker containers:**
    ```bash
    docker-compose up -d

## Usage

### Login

Use curl or Postman to login with default details:

    curl -X POST http://localhost:3000/login \
    -H "Content-Type: application/json" \
    -d '{"username": "user1", "password": "password1"}

### Making Requests

Use curl or Postman to make requests to the API. Here is an example using curl:

    curl -X GET 'http://localhost:3000/contacts?page=1&limit=10' \
    -H "Authorization: Bearer <Your_JWT_Here>" \
    -H "Content-Type: application/json"
