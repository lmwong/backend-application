// migrations/populateContacts.ts
import connectDB from '../src/db/mongoClient';
import Contact from '../src/models/Contact';

const populateContacts = async () => {
    await connectDB();

    // Create a bunch of dummy contacts
    const contacts = Array.from({ length: 5000 }, (_, i) => ({
        username: `user${i}`,
        phoneNumber: `100000${i}`
    }));

    await Contact.deleteMany(); // Clear existing data
    await Contact.insertMany(contacts);
    console.log('Database has been populated with sample data.');

    process.exit();
};

populateContacts();
